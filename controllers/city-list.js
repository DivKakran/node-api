const CityList     = require('../models/city-list');
const Language     = require('../models/language');
const mon          = require('../models/monumentRes');    
const Monument     = require('../models/monuments');
const Utilfunction = require("../constants/UtilFunction");

exports.getCityList = async function(req , res){
    var responseToDeliver = new Object();
    responseToDeliver.status = Utilfunction.getStatusObject("Data Available" , 200 , false);
    if(responseToDeliver.status.errorStatus) {
        res.json(responseToDeliver);
    }else{
        CityList.find({} , async (error , cityList) => {
            let response = JSON.parse(JSON.stringify(cityList));
            var resForSent = [];
            var lang       = [];
            await Language.find({} , (err , data) => {
                let _temp = JSON.parse(JSON.stringify(data));
                for(let item of _temp){
                    let obj = {
                        languageId   : item._id,
                        languageName : item.language_name
                    }
                    lang.push(obj)
                }
            });
            for(let item of response){
               await Monument.find({monument_city_id:item._id} , (err , data) =>{
                   let _temp = JSON.parse(JSON.stringify(data));
                   let monumentArray = []
                   for(let item of _temp){
                       if(item.monument_description == undefined){
                           item.monument_description = 'I will add description later';
                       }
                        let monObj = new mon (
                            item._id,
                            item.image_name,
                            item.monument_description,
                            item.monument_city_id,
                            item.monument_name
                        )
                        monumentArray.push(monObj);
                   }
     
                    var obj = {
                        id: item._id,
                        city_name: item.city_name,
                        language:lang,
                        monument: monumentArray
                    }; 
                    resForSent.push(obj);
                });
            }
            responseToDeliver.city = resForSent;
            res.json(responseToDeliver);
        });
    }
}